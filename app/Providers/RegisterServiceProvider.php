<?php


namespace App\Providers;


use App\Id\Generator\PhpUniqueIdGenerator;
use App\PasswordHash\PasswordHashGenerator;
use App\Register\Register;
use App\Register\RegisterRequestValidator;
use App\User\Repository\MongoUserPersistRepository;
use App\User\Repository\UserFetchRepository;
use App\UserMappers\RegisterRequestToUserMapper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Jenssegers\Mongodb\Connection;

class RegisterServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(Register::class, function ($app) {
            /** @var Connection $connection */
            $connection = DB::connection('mongodb');

            $idGenerator = new PhpUniqueIdGenerator();

            $validator = new RegisterRequestValidator();

            $registerReqToUserMapper = new RegisterRequestToUserMapper();

            $mongoUserCreateRepo = new MongoUserPersistRepository($connection);

            $fetchRepo = $app->make(UserFetchRepository::class);

            $hashGenerator = $app->make(PasswordHashGenerator::class);

            return new Register(
                $validator,
                $idGenerator,
                $mongoUserCreateRepo,
                $registerReqToUserMapper,
                $fetchRepo,
                $hashGenerator
            );
        });
    }
}
