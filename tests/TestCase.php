<?php

namespace Tests;

use App\User;
use App\UserActivation\UserActivationCode;
use Laravel\Lumen\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__ . '/../bootstrap/app.php';
    }

    protected function createTestUser(bool $confirmed = false)
    {
        return new User(
            '1',
            'test@user.com',
            '$2y$10$GCiuoav3buBjdDNu.4HGAOXOPWzJTD2aLg.6hWU7N.PtBeUjplcPa',
            $confirmed
        );
    }

    protected function createUserActivationCode(bool $used = false): UserActivationCode
    {
        return new UserActivationCode(
            'code',
            '1',
            $used
        );
    }
}
