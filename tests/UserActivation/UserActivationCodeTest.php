<?php


namespace Tests\UserActivation;


use App\UserActivation\UserActivationCode;
use App\UserActivation\UserActivationCodeMarkUsedException;
use Tests\TestCase;

class UserActivationCodeTest extends TestCase
{
    private UserActivationCode $unusedUserCode;

    private UserActivationCode $usedUserCode;

    public function setUp(): void
    {
        parent::setUp();

        $this->usedUserCode = $this->createUserActivationCode(true);

        $this->unusedUserCode = $this->createUserActivationCode();
    }

    /**
     * @throws UserActivationCodeMarkUsedException
     */
    public function testUnusedCodeMarkAsUsed()
    {
        $this->unusedUserCode->markAsUsed();

        $isUsed = $this->unusedUserCode->isUsed();

        $this->assertTrue($isUsed);
    }

    /**
     * @throws UserActivationCodeMarkUsedException
     */
    public function testUsedCodeCannotBeMarkedAsUsed()
    {
        $this->expectException(UserActivationCodeMarkUsedException::class);

        $this->usedUserCode->markAsUsed();
    }
}
