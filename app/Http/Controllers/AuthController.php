<?php

namespace App\Http\Controllers;

use App\Auth\AuthInvalidCredException;
use App\User\UserLoginUserCase;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    private UserLoginUserCase $userLoginUserCase;

    public function __construct(UserLoginUserCase $userLoginUserCase)
    {
        $this->userLoginUserCase = $userLoginUserCase;
    }

    public function auth(Request $request)
    {
        try {
            $password = $request->input('password');

            $email = $request->input('email');

            $token = $this->userLoginUserCase->auth($password, $email);

            return response()->json(['user_id' => $token->userId(), 'token' => $token->token()]);
        } catch (AuthInvalidCredException $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
}
