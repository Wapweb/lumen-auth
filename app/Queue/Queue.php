<?php


namespace App\Queue;


interface Queue
{
    /**
     * @param int $messagesCount
     * @return QueueMessage[]
     */
    public function fetch(int $messagesCount): array;

    public function add(QueueMessage $message);
}
