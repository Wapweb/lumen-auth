<?php


namespace App\Id;


interface IdGenerator
{
    function generate(): string;
}
