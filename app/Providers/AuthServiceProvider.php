<?php

namespace App\Providers;

use App\Auth\TokenAuth;
use App\User;
use App\User\Repository\UserFetchRepository;
use App\User\Repository\UserNotFoundException;
use App\User\UserTokenCheckUserCase;
use App\UserToken\Repository\UserTokenNotFound;
use App\UserToken\Repository\UserTokenRepository;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;

class AuthServiceProvider extends ServiceProvider
{
    private const HEADER_USER_ID = 'User-Id';

    private const HEADER_USER_TOKEN = 'User-Token';

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.



        $this->app['auth']->viaRequest('api', function ($request) {
            /** @var Request $request */
            $userId = $request->header(self::HEADER_USER_ID);
            $userToken = $request->header(self::HEADER_USER_TOKEN);

            /** @var TokenAuth $tokenAuth */
            $tokenAuth = $this->app->get(TokenAuth::class);

            $isValid = $tokenAuth->authenticate([
                'user_id' => $userId,
                'token' => $userToken
            ]);

            return $isValid ? $tokenAuth->getIdentity() : null;
        });
    }
}
