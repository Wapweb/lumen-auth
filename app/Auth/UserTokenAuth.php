<?php


namespace App\Auth;


use App\User\Repository\UserFetchRepository;
use App\User\Repository\UserNotFoundException;
use App\UserToken\Repository\UserTokenNotFound;
use App\UserToken\Repository\UserTokenRepository;

class UserTokenAuth implements TokenAuth
{
    private UserFetchRepository $userFetchByIdRepository;

    private UserTokenRepository $userTokenRepository;

    private $identity = null;

    public function __construct(UserFetchRepository $userFetchByIdRepository, UserTokenRepository $userTokenRepository)
    {
        $this->userTokenRepository = $userTokenRepository;
        $this->userFetchByIdRepository = $userFetchByIdRepository;
    }

    public function authenticate(array $cred = null): bool
    {
        if (empty($cred) || !isset($cred['user_id']) || !isset($cred['token'])) {
            throw new AuthException('Invalid $cred: password and email fields are required');
        }

        $userId = $cred['user_id'];

        $token = $cred['token'];

        if(empty($userId) || empty($token)) {
            return  false;
        }

        try {
            $user = $this->userFetchByIdRepository->fetchById($userId);

            $userToken = $this->userTokenRepository->fetchByUserIdAndToken($user->id(), $token);

            $this->identity = $user;

            return  true;
        } catch (UserNotFoundException|UserTokenNotFound $e) {
            return false;
        } catch (\Exception $e) {
            throw new AuthException($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getIdentity()
    {
        return $this->identity;
    }
}
