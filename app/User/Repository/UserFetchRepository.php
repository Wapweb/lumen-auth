<?php


namespace App\User\Repository;


use App\User;

interface UserFetchRepository
{
    /**
     * @param string $id
     * @return User
     * @throws UserNotFoundException
     */
    function fetchById(string $id): User;

    /**
     * @param string $email
     * @return User
     * @throws UserNotFoundException
     */
    function fetchByEmail(string $email): User;
}
