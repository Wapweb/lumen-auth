<?php


namespace App\Register;


use Throwable;

class RequestValidationException extends \Exception
{
    /**
     * RequestValidationException constructor.
     * @param string|array $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if (is_array($message)) {
            $message = implode(', ', $message);
        }

        parent::__construct($message, $code, $previous);
    }
}
