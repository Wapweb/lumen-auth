<?php


namespace Tests\Id\Generator;


use App\Id\IdGenerator;
use Tests\TestCase;

class PhpUniqueIdGeneratorTest extends TestCase
{
    private IdGenerator $idGenerator;

    protected function setUp(): void
    {
        parent::setUp();

        $this->idGenerator = $this->app->make(IdGenerator::class);
    }

    public function testGeneration()
    {
        $id = $this->idGenerator->generate();

        $this->assertNotEmpty($id);
    }
}
