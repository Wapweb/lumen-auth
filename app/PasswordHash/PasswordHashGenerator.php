<?php


namespace App\PasswordHash;


interface PasswordHashGenerator
{
    function generate(string $password): string;
}
