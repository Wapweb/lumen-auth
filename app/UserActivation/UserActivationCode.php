<?php


namespace App\UserActivation;


class UserActivationCode
{
    private string $code;
    private string $userId;
    private bool $used = false;

    public function __construct(string $code, string $userId, bool $used = false)
    {
        $this->code = $code;
        $this->userId = $userId;
        $this->used = $used;
    }

    public function code(): string
    {
        return $this->code;
    }

    public function userId(): string
    {
        return $this->userId;
    }

    public function isUsed(): bool
    {
        return $this->used;
    }

    public function markAsUsed()
    {
        if ($this->isUsed()) {
            throw new UserActivationCodeMarkUsedException('Code has been already used');
        }

        $this->used = true;
    }
}
