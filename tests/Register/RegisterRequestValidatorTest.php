<?php


namespace Tests\Register;


use App\Register\RegisterRequest;
use App\Register\RegisterRequestValidator;
use Tests\TestCase;

class RegisterRequestValidatorTest extends TestCase
{
    private RegisterRequestValidator $registerReqValidator;

    public function setUp(): void
    {
        parent::setUp();

        $this->registerReqValidator = $this->app->make(RegisterRequestValidator::class);
    }

    /**
     * @param RegisterRequest $request
     * @dataProvider invalidRegisterRequestDataProvider
     */
    public function testInvalid(RegisterRequest $request)
    {
        $result = $this->registerReqValidator->isValid($request);

        $this->assertNotTrue($result);
    }

    /**
     * @param RegisterRequest $request
     * @dataProvider validRegisterRequestDataProvider
     */
    public function testValid(RegisterRequest $request)
    {
        $result = $this->registerReqValidator->isValid($request);

        $this->assertTrue($result);
    }

    public function validRegisterRequestDataProvider()
    {
        return [
            [new RegisterRequest('test@gmail.com', 'qwertyqwerty')],
            [new RegisterRequest('test+test2@gmail.com', '11111111')],
        ];
    }

    public function invalidRegisterRequestDataProvider()
    {
        return [
            [new RegisterRequest('', 'qwertyqwerty')],
            [new RegisterRequest('test@gmail.com', '')],
            [new RegisterRequest('testgmail.com', 'qwertyqwerty')],
            [new RegisterRequest('', '')],
        ];
    }
}
