<?php


namespace App\Http\Controllers;


use App\UserActivation\UserActivationVerify;

class UserActivationController extends Controller
{
    private UserActivationVerify $userActivationVerify;

    public function __construct(UserActivationVerify $userActivationVerify)
    {
        $this->userActivationVerify = $userActivationVerify;
    }

    public function verify($code)
    {
        try {
            $this->userActivationVerify->verify($code);
            return response()->json(['message' => 'ok']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
}
