<?php


namespace Tests;


use App\PasswordHash\BcryptPasswordHashGenerator;

class BcryptPasswordHashGeneratorTest extends TestCase
{
    private BcryptPasswordHashGenerator $hashGenerator;

    public function setUp(): void
    {
        parent::setUp();

        $this->hashGenerator = $this->app->make(BcryptPasswordHashGenerator::class);
    }

    /**
     * @param string $password
     * @dataProvider passwordDataProvider
     */
    public function testGenerateNotEmpty(string $password)
    {
        $passwordHash = $this->hashGenerator->generate($password);

        $this->assertNotEmpty($passwordHash);
    }

    /**
     * @param string $password
     * @dataProvider passwordDataProvider
     */
    public function testGenerateNotEqualToPassword(string $password)
    {
        $passwordHash = $this->hashGenerator->generate($password);

        $this->assertNotEquals($passwordHash, $password);
    }

    public function passwordDataProvider()
    {
        return [
            ['11111111'],
            ['']
        ];
    }
}
