<?php


namespace App\UserMappers;


use App\User;

class UserToUserCreatedJsonResponseMapper
{
    public function map(User $user): array
    {
        return  [
            'id' => $user->id(),
            'email' => $user->email()
        ];
    }
}
