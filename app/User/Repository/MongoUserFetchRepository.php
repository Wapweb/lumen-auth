<?php


namespace App\User\Repository;


use App\Id\Id;
use App\Id\IdFactory;
use App\Id\SimpleId;
use App\User;
use Jenssegers\Mongodb\Connection;

class MongoUserFetchRepository implements UserFetchRepository
{
    private Connection $connection;

    private string $collectionName = 'user';

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string $id
     * @return User
     * @throws UserNotFoundException
     */
    public function fetchById(string $id): User
    {
        $data = $this->connection->collection($this->collectionName)->where('id', $id)->first();

        if (empty($data)) {
            throw new UserNotFoundException(sprintf('User with id `%s` not found', $id));
        }

        return $this->mapToModel($data);
    }

    /**
     * @param string $email
     * @return User
     * @throws UserNotFoundException
     */
    public function fetchByEmail(string $email): User
    {
        $data = $this->connection->collection($this->collectionName)->where('email', $email)->first();

        if (empty($data)) {
            throw new UserNotFoundException(sprintf('User with email `%s` not found', $email));
        }

        return $this->mapToModel($data);
    }

    private function mapToModel(array $data): User
    {
        return new User($data['id'], $data['email'], $data['password']);
    }
}
