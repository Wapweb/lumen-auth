<?php


namespace App\UserActivation\Repository;


use App\UserActivation\UserActivationCode;

interface UserActivationRepository
{
    /**
     * @param string $code
     * @return UserActivationCode
     * @throws UserActivationCodeNotFound
     */
    function fetchByCode(string $code): UserActivationCode;

    function persist(UserActivationCode $userActivationCode);
}
