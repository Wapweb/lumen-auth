<?php


namespace Tests\Auth;


use App\Auth\AuthException;
use App\Auth\EmailPasswordLoginAuth;
use App\User;
use App\User\Repository\UserFetchRepository;
use Mockery\Mock;
use Tests\TestCase;

class MongoEmailPasswordLoginAuthTest extends TestCase
{
    private EmailPasswordLoginAuth $mongoEmailPassLoginAuth;

    private User $testUser;

    public function setUp(): void
    {
        parent::setUp();

        $this->testUser = $this->createTestUser();

        $this->app->instance(UserFetchRepository::class, \Mockery::mock(UserFetchRepository::class, function ($mock) {
            /** @var Mock $mock */
            $mock->shouldReceive('fetchByEmail')
                ->withArgs([$this->testUser->email()])
                ->andReturn($this->testUser);
        }));

        $this->mongoEmailPassLoginAuth = $this->app->make(EmailPasswordLoginAuth::class);
    }

    /**
     * @param array $cred
     * @throws AuthException
     * @dataProvider validCredDataProvider
     */
    public function testSuccessAuth(array $cred)
    {
        $result = $this->mongoEmailPassLoginAuth->authenticate($cred);

        $this->assertTrue($result);
    }

    /**
     * @param array $cred
     * @throws AuthException
     * @dataProvider invalidCredDataProvider
     */
    public function testFailAuth(array $cred)
    {
        $result = $this->mongoEmailPassLoginAuth->authenticate($cred);

        $this->assertNotTrue($result);
    }

    /**
     * @param $cred
     * @throws AuthException
     * @dataProvider invalidCredInputDataProvider
     */
    public function testInvalidCredInput($cred)
    {
        $this->expectException(AuthException::class);
        $this->mongoEmailPassLoginAuth->authenticate($cred);
    }

    public function validCredDataProvider()
    {
        return [
            [[
                'email' => 'test@user.com',
                'password' => '11111111'
            ]]
        ];
    }

    public function invalidCredDataProvider()
    {
        return [
            [[
                'email' => 'test@user.com',
                'password' => '1'
            ]],
            [[
                'email' => 'tes2t@user.com',
                'password' => '0'
            ]],
            [[
                'email' => 'tes2t@user.com',
                'password' => '11111111'
            ]]
        ];
    }

    public function invalidCredInputDataProvider()
    {
        return [
            [[
                'email' => null,
                'password' => '11111111'
            ]],
            [[
                'email' => 'test@user.com',
                'password' => null
            ]],
            [[
                'email' => null,
                'password' => null
            ]],
            [[
            ]]
        ];
    }
}
