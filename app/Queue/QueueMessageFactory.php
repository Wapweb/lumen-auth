<?php


namespace App\Queue;


class QueueMessageFactory
{
    public function createByPayload(array $payload = null): QueueMessage
    {
        return new QueueMessage($payload);
    }
}
