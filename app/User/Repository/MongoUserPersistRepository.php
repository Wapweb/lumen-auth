<?php


namespace App\User\Repository;


use App\Id\Id;
use App\User;
use Jenssegers\Mongodb\Connection;

class MongoUserPersistRepository implements UserPersistRepository
{
    private Connection $connection;

    private string $collectionName = 'user';

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param User $user
     * @return bool
     * @throws UserPersistException
     */
    public function persist(User $user): bool
    {
        $data = $this->mapUserToMongoData($user);

        $result = $this->connection->collection($this->collectionName)
            ->where('id', $user->id())
            ->update($data, ['upsert' => true]);

        if (!$result) {
            throw new UserPersistException('Failed to persist user');
        }

        return true;
    }

    private function mapUserToMongoData(User $user): array
    {
        return [
            'email' => $user->email(),
            'password' => $user->password(),
            'id' => $user->id(),
            'status' => $user->status()
        ];
    }
}
