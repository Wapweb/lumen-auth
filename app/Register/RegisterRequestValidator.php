<?php


namespace App\Register;


final class RegisterRequestValidator
{
    private array $rules = [
        'password' => [
            'minLen' => 6,
            'maxLen' => 100
        ]
    ];

    private array $messages = [];

    public function isValid(RegisterRequest $registerRequest): bool
    {
        $this->clearMessages();

        $password = $registerRequest->password();
        if (!$this->validatePassword($password)) {
            $this->addMessage('Invalid password');

            return false;
        }

        $email = $registerRequest->email();
        if (!$this->validateEmail($email)) {
            $this->addMessage('Invalid email');

            return false;
        }

        return true;
    }

    public function getMessages(): array
    {
        return  $this->messages;
    }

    private function validatePassword(string $password): bool
    {
        if (empty($password)) {
            return false;
        }

        $minLen = $this->rules['password']['minLen'];

        $maxLen = $this->rules['password']['maxLen'];

        $passLen = mb_strlen($password, 'UTF-8');

        if ($passLen > $maxLen || $passLen < $minLen) {
            return false;
        }

        return true;
    }

    private function validateEmail(string $email): bool
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    private function addMessage(string $message)
    {
        $this->messages[] = $message;
    }

    private function clearMessages()
    {
        $this->messages = [];
    }
}
