<?php


namespace App\User;


use App\Register\Register;
use App\Register\RegisterRequestFactory;
use App\Register\RequestValidationException;
use App\Register\PersistenceException;
use App\Token\TokenGeneratorException;
use App\UserActivation\UserActivationSend;
use App\UserActivation\UserActivationSendFailedException;
use App\UserMappers\UserToUserCreatedJsonResponseMapper;
use Illuminate\Http\Request;

class CreateNewUserUseCase
{
    private Register $registerService;

    private RegisterRequestFactory $registerRequestFactory;

    private UserToUserCreatedJsonResponseMapper $userToJsonMapper;

    private UserActivationSend $userActivationSend;

    /**
     * Create a new controller instance.
     *
     * @param Register $registerService
     * @param RegisterRequestFactory $registerRequestFactory
     * @param UserToUserCreatedJsonResponseMapper $userToJsonMapper
     * @param UserActivationSend $activationEmailService
     */
    public function __construct(
        Register $registerService,
        RegisterRequestFactory $registerRequestFactory,
        UserToUserCreatedJsonResponseMapper $userToJsonMapper,
        UserActivationSend $activationEmailService
    )
    {
        $this->registerService = $registerService;
        $this->registerRequestFactory = $registerRequestFactory;
        $this->userToJsonMapper = $userToJsonMapper;
        $this->userActivationSend = $activationEmailService;
    }

    /**
     * @param Request $request
     * @return array
     * @throws UserActivationSendFailedException
     * @throws PersistenceException
     * @throws RequestValidationException
     * @throws TokenGeneratorException
     */
    public function execute(Request $request): array
    {
        /** @var array $params */
        $params = $request->all();

        $registerRequest = $this->registerRequestFactory->fromArray($params);

        $user = $this->registerService->register($registerRequest);

        $this->userActivationSend->send($user);

        return $this->userToJsonMapper->map($user);
    }
}
