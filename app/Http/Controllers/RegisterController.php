<?php


namespace App\Http\Controllers;

use App\Register\RequestValidationException;
use App\User\CreateNewUserUseCase;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    private CreateNewUserUseCase $userCreateNewUseCase;

    /**
     * Create a new controller instance.
     *
     * @param CreateNewUserUseCase $userCreateNewUseCase
     */
    public function __construct(CreateNewUserUseCase $userCreateNewUseCase)
    {
        $this->userCreateNewUseCase = $userCreateNewUseCase;
    }

    public function register(Request $request)
    {
        try {
            $response = $this->userCreateNewUseCase->execute($request);

            return response()->json($response, 201);
        } catch (RequestValidationException $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
}
