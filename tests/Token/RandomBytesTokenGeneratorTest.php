<?php


namespace Tests\Token;


use App\Token\RandomBytesTokenGenerator;
use App\Token\TokenGeneratorException;
use Tests\TestCase;

class RandomBytesTokenGeneratorTest extends TestCase
{
    private RandomBytesTokenGenerator $randomBytesTokenGenerator;

    public function setUp(): void
    {
        parent::setUp();

        $this->randomBytesTokenGenerator = $this->app->make(RandomBytesTokenGenerator::class);
    }

    /**
     * @throws TokenGeneratorException
     */
    public function testNotEmpty()
    {
        $token = $this->randomBytesTokenGenerator->generate();

        $this->assertNotEmpty($token);
    }

    /**
     * @throws TokenGeneratorException
     */
    public function testCorrectLength()
    {
        $token = $this->randomBytesTokenGenerator->generate();

        $tokenLen = strlen($token);

        $this->assertEquals(RandomBytesTokenGenerator::LENGTH * 2, $tokenLen);
    }
}
