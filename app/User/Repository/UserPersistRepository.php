<?php


namespace App\User\Repository;


use App\User;

interface UserPersistRepository
{
    function persist(User $user);
}
