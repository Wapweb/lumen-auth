<?php


namespace App\UserActivation;


use App\User\Repository\UserFetchRepository;
use App\User\Repository\UserNotFoundException;
use App\User\Repository\UserPersistRepository;
use App\User\UserAlreadyConfirmedException;
use App\UserActivation\Repository\UserActivationRepository;

class UserActivationVerify
{
    private UserActivationRepository $userActivationRepository;

    private UserFetchRepository $userFetchRepository;

    private UserPersistRepository $userPersistRepository;

    public function __construct(
        UserActivationRepository $activationRepository,
        UserFetchRepository $fetchRepository,
        UserPersistRepository $persistRepository
    )
    {
        $this->userActivationRepository = $activationRepository;
        $this->userFetchRepository = $fetchRepository;
        $this->userPersistRepository = $persistRepository;
    }

    /**
     * @param string $code
     * @throws Repository\UserActivationCodeNotFound
     * @throws UserActivationCodeMarkUsedException
     * @throws UserNotFoundException
     * @throws UserAlreadyConfirmedException
     */
    public function verify(string $code)
    {
        $codeModel = $this->userActivationRepository->fetchByCode($code);

        $user = $this->userFetchRepository->fetchById($codeModel->userId());

        $user->confirm();

        $codeModel->markAsUsed();

        $this->userActivationRepository->persist($codeModel);

        $this->userPersistRepository->persist($user);
    }
}
