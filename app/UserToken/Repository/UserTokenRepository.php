<?php


namespace App\UserToken\Repository;


use App\UserToken\UserToken;

interface UserTokenRepository
{
    /**
     * @param string $token
     * @return UserToken
     * @throws UserTokenNotFound
     */
    function fetchByToken(string $token): UserToken;

    /**
     * @param UserToken $token
     * @return bool
     * @throws UserTokenPersistException
     */
    function persist(UserToken $token): bool;

    /**
     * @param string $userId
     * @param string $token
     * @return UserToken
     * @throws UserTokenNotFound
     */
    function fetchByUserIdAndToken(string $userId, string $token): UserToken;
}
