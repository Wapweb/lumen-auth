<?php


namespace App\Register;


final class RegisterRequestFactory
{
    public function fromArray(array $data): RegisterRequest
    {
        $pass = $data['password'] ?? '';
        $email = $data['email'] ?? '';

        return new RegisterRequest($email, $pass);
    }
}
