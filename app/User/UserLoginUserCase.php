<?php


namespace App\User;


use App\Auth\AuthException;
use App\Auth\AuthInvalidCredException;
use App\Auth\LoginAuth;
use App\Token\TokenGenerator;
use App\Token\TokenGeneratorException;
use App\User;
use App\UserToken\Repository\UserTokenPersistException;
use App\UserToken\Repository\UserTokenRepository;
use App\UserToken\UserToken;
use App\UserToken\UserTokenFactory;
use Illuminate\Http\Request;

class UserLoginUserCase
{
    private LoginAuth $auth;

    private TokenGenerator $tokenGenerator;

    private UserTokenRepository $userTokenRepository;

    private UserTokenFactory $userTokenFactory;

    public function __construct(
        LoginAuth $auth,
        TokenGenerator $tokenGenerator,
        UserTokenRepository $userTokenRepository,
        UserTokenFactory $userTokenFactory)
    {
        $this->auth = $auth;
        $this->tokenGenerator = $tokenGenerator;
        $this->userTokenFactory = $userTokenFactory;
        $this->userTokenRepository = $userTokenRepository;
    }

    /**
     * @param string $password
     * @param string $email
     * @return UserToken
     * @throws AuthException
     * @throws AuthInvalidCredException
     * @throws TokenGeneratorException
     * @throws UserTokenPersistException
     */
    public function auth(string $password, string $email): UserToken
    {
        $cred = [
            'password' => $password,
            'email' => $email
        ];

        $valid = $this->auth->authenticate($cred);

        if (!$valid) {
            throw new AuthInvalidCredException('Invalid credentials');
        }

        $token = $this->tokenGenerator->generate();

        /** @var User $user */
        $user = $this->auth->getIdentity();

        $tokenModel = $this->userTokenFactory->create($token, $user->id());

        $this->userTokenRepository->persist($tokenModel);

        return $tokenModel;
    }
}
