<?php


namespace Tests;

use App\PasswordHash\BcryptPasswordHashVerify;

class BcryptPasswordHashVerifyTest extends TestCase
{
    private BcryptPasswordHashVerify $hashVerify;

    public function setUp(): void
    {
        parent::setUp();

        $this->hashVerify = $this->app->make(BcryptPasswordHashVerify::class);
    }

    /**
     * @param string $hash
     * @param string $password
     * @dataProvider validPassHashPairsDataProvider
     */
    public function testVerifyValid(string $hash, string $password)
    {
        $result = $this->hashVerify->verify($password, $hash);

        $this->assertTrue($result);
    }

    /**
     * @param string $hash
     * @param string $password
     * @dataProvider invalidPassHashPairsDataProvider
     */
    public function testVerifyInvalid(string $hash, string $password)
    {
        $result = $this->hashVerify->verify($password, $hash);

        $this->assertNotTrue($result);
    }


    public function validPassHashPairsDataProvider()
    {
        return [
            ['$2y$10$GCiuoav3buBjdDNu.4HGAOXOPWzJTD2aLg.6hWU7N.PtBeUjplcPa', '11111111']
        ];
    }

    public function invalidPassHashPairsDataProvider()
    {
        return [
            ['$2y$10$GCiuoav3buBjdDNu.4HGAOXOPWzJTD2aLg.6hWU7N.PtBeUjplcPa', '11111112']
        ];
    }
}
