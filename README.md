# Lumen auth test app

## Requirements

- docker

## Quick start

- Copy .env.example to .env
```cp env.example .env```
- Init project
 ```bash vessel init```
- Run docker
```./vessel start```
 - Install dependencies
```./vessel exec app bash -c 'composer install'```

## Tests
```./vessel test```
