<?php


namespace App\Queue\QueueAdapters\MongoQueueAdapter;


use App\Queue\Queue;
use App\Queue\QueueMessage;

class MongoQueueAdapter implements Queue
{
    private MongoQueueAdapterRepository $repository;

    public function __construct(MongoQueueAdapterRepository $repository)
    {
        $this->repository = $repository;
    }

    public function add(QueueMessage $message)
    {
       $this->repository->add($message);
    }

    /**
     * @param int $messagesCount
     * @return QueueMessage[]
     */
    public function fetch(int $messagesCount): array
    {
        return  $this->repository->fetch($messagesCount);
    }
}
