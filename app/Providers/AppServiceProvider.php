<?php

namespace App\Providers;

use App\Auth\LoginAuth;
use App\Auth\EmailPasswordLoginAuth;
use App\Auth\TokenAuth;
use App\Auth\UserTokenAuth;
use App\Id\Generator\PhpUniqueIdGenerator;
use App\Id\IdGenerator;
use App\PasswordHash\BcryptPasswordHashGenerator;
use App\PasswordHash\BcryptPasswordHashVerify;
use App\PasswordHash\PasswordHashGenerator;
use App\PasswordHash\PasswordHashVerify;
use App\Queue\Queue;
use App\Queue\QueueAdapters\MongoQueueAdapter\MongoQueueAdapter;
use App\Queue\QueueAdapters\MongoQueueAdapter\MongoQueueAdapterRepository;
use App\Register\RegisterRequestFactory;
use App\Token\RandomBytesTokenGenerator;
use App\Token\TokenGenerator;
use App\User\Repository\MongoUserPersistRepository;
use App\User\Repository\MongoUserFetchRepository;
use App\User\Repository\UserPersistRepository;
use App\User\Repository\UserFetchRepository;
use App\UserActivation\Repository\MongoUserActivationRepository;
use App\UserActivation\Repository\UserActivationRepository;
use App\UserMappers\UserToUserCreatedJsonResponseMapper;
use App\UserToken\Repository\MongoUserTokenRepository;
use App\UserToken\Repository\UserTokenRepository;
use App\UserToken\UserTokenFactory;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Jenssegers\Mongodb\Connection;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(RegisterRequestFactory::class, function ($app) {
            return new RegisterRequestFactory();
        });

        $this->app->bind(UserToUserCreatedJsonResponseMapper::class, function ($app) {
            return new UserToUserCreatedJsonResponseMapper();
        });

        $this->app->bind(IdGenerator::class, function ($app) {
            return new PhpUniqueIdGenerator();
        });

        $this->app->singleton(Connection::class, function ($app) {
            return DB::connection('mongodb');
        });

        $this->app->bind(UserFetchRepository::class, function ($app) {
            $db = $this->app->make(Connection::class);

            return new MongoUserFetchRepository($db);
        });

        $this->app->bind(UserPersistRepository::class, function ($app) {
            $db = $this->app->make(Connection::class);

            return new MongoUserPersistRepository($db);
        });

        $this->app->singleton(TokenGenerator::class, function ($app) {
            return new RandomBytesTokenGenerator();
        });

        $this->app->bind(UserTokenRepository::class, function ($app) {
            $db = $this->app->make(Connection::class);

            return new MongoUserTokenRepository($db, new UserTokenFactory());
        });

        $this->app->singleton(PasswordHashGenerator::class, function ($app) {
            return new BcryptPasswordHashGenerator();
        });

        $this->app->singleton(PasswordHashVerify::class, function ($app) {
            return new BcryptPasswordHashVerify();
        });

        $this->app->singleton(UserActivationRepository::class, function ($app) {
            $db = $this->app->make(Connection::class);

            return new MongoUserActivationRepository($db);
        });

        $this->app->singleton(Queue::class, function ($app) {
            $db = $this->app->make(Connection::class);

            $repo = new MongoQueueAdapterRepository($db);

            return new MongoQueueAdapter($repo);
        });

        $this->app->bind(LoginAuth::class, function ($app) {
            $repo = $this->app->make(UserFetchRepository::class);

            $passVerify = $this->app->make(PasswordHashVerify::class);

            return new EmailPasswordLoginAuth($repo, $passVerify);
        });

        $this->app->bind(TokenAuth::class, function ($app) {
            $repo = $this->app->make(UserFetchRepository::class);

            $userTokenRepo = $this->app->make(UserTokenRepository::class);

            return new UserTokenAuth($repo, $userTokenRepo);
        });
    }
}
