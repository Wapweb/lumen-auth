<?php


namespace App\UserMappers;


use App\User;

class UserToUserJsonResponseMapper
{
    public function map(User $user): array
    {
        return  [
            'id' => $user->id(),
            'email' => $user->password()
        ];
    }
}
