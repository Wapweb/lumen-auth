<?php


namespace App\UserActivation\Repository;


use App\UserActivation\UserActivationCode;
use App\UserActivation\Repository\UserActivationCodeNotFound;
use App\UserToken\UserToken;
use Jenssegers\Mongodb\Connection;
use MongoDB\BSON\UTCDateTime;

class MongoUserActivationRepository implements UserActivationRepository
{
    private Connection $connection;

    private string $collectionName = 'user_activation_code';

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string $code
     * @return UserActivationCode
     * @throws UserActivationCodeNotFound
     */
    public function fetchByCode(string $code): UserActivationCode
    {
        $data = $this->connection->collection($this->collectionName)->where('code', $code)->first();

        if (empty($data)) {
            throw new UserActivationCodeNotFound(sprintf('User code `%s` not found', $code));
        }

        return $this->mapToModel($data);
    }

    /**
     * @param UserActivationCode $userActivationCode
     * @return bool
     * @throws UserActivationCodePersistException
     */
    public function persist(UserActivationCode $userActivationCode)
    {
        $data = $this->mapToData($userActivationCode);

        $result = $this->connection->collection($this->collectionName)
            ->where('user_id', $userActivationCode->userId())
            ->where('code', $userActivationCode->code())
            ->update($data, ['upsert' => true]);

        if (!$result) {
            throw new UserActivationCodePersistException('Failed to persist user');
        }

        return true;
    }

    private function mapToModel(array $data): UserActivationCode
    {
        return new UserActivationCode($data['code'], $data['user_id'], (bool)$data['used']);
    }

    private function mapToData(UserActivationCode $userActivationCode)
    {
        return [
            'code' => $userActivationCode->code(),
            'user_id' => $userActivationCode->userId(),
            'used' => $userActivationCode->isUsed()
        ];
    }
}
