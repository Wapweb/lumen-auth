<?php


namespace App\UserActivation;


use App\Queue\Queue;
use App\Queue\QueueMessageFactory;
use App\Token\TokenGenerator;
use App\Token\TokenGeneratorException;
use App\User;
use App\UserActivation\Repository\UserActivationRepository;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;
use Prophecy\Exception\Prediction\FailedPredictionException;

class UserActivationSend
{
    private Queue $queue;

    private TokenGenerator $tokenGenerator;

    private UserActivationRepository $userActivationRepository;

    private QueueMessageFactory $queueMessageFactory;

    public function __construct(
        Queue $queue,
        TokenGenerator $tokenGenerator,
        UserActivationRepository $activationRepository,
        QueueMessageFactory $queueMessageFactory
    )
    {
        $this->queue = $queue;
        $this->tokenGenerator = $tokenGenerator;
        $this->userActivationRepository = $activationRepository;
        $this->queueMessageFactory = $queueMessageFactory;
    }

    /**
     * @param User $user
     * @throws UserActivationSendFailedException
     * @throws TokenGeneratorException
     */
    public function send(User $user)
    {
        $this->ensureUserCanReceiveActivationCode($user);

        $activationCode = $this->createUserActivationCode($user);

        $queueMessage = $this->createQueueMessage($user, $activationCode);

        try {
            $this->userActivationRepository->persist($activationCode);

            $this->queue->add($queueMessage);
        } catch (\Exception $e) {
            throw new FailedPredictionException($e->getMessage());
        }
    }

    private function createQueueMessage(User $user, UserActivationCode $code)
    {
        $payload = [
            'action' => 'mail',
            'type' => 'activation',
            'data' => [
                'user_id' => $user->id(),
                'activation_code' => $code->code()
            ]
        ];

        return $this->queueMessageFactory->createByPayload($payload);
    }

    /**
     * @param User $user
     * @return UserActivationCode
     * @throws TokenGeneratorException
     */
    private function createUserActivationCode(User $user): UserActivationCode
    {
        $activationCode = $this->tokenGenerator->generate();

        return new UserActivationCode($activationCode, $user->id());
    }

    /**
     * @param User $user
     * @throws UserActivationSendFailedException
     */
    private function ensureUserCanReceiveActivationCode(User $user)
    {
        if ($user->isConfirmed()) {
            throw new UserActivationSendFailedException('Activation code cannot be send to confirmed user account');
        }
    }
}
