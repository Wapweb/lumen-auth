<?php


namespace App\Console\Commands;


use App\Queue\Queue;
use App\Queue\QueueMessage;
use App\User;
use App\User\Repository\UserFetchRepository;
use App\UserActivation\UserActivationSend;
use Illuminate\Console\Command;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;

class MailQueue extends Command
{
    protected $name = 'mail-queue';

    private int $messagesLimit = 5;

    private int $delaySecs = 5;

    private string $bodyTemplate = 'Your activation code is %s';

    private string $subject = 'Activation code';

    public function handle(
        Queue $queue,
        UserFetchRepository $userFetchRepository,
        Mailer $mailer
    )
    {
        while (true) {
            $messages = $queue->fetch($this->messagesLimit);

            foreach ($messages as $message) {
                try {
                    $this->handleMessage($message, $userFetchRepository, $mailer);
                } catch (\Exception $e) {
                    // todo logging
                }
            }

            sleep($this->delaySecs);
        }
    }

    /**
     * @param QueueMessage $message
     * @param UserFetchRepository $userFetchRepository
     * @param Mailer $mailer
     * @throws User\Repository\UserNotFoundException
     */
    private function handleMessage(QueueMessage $message, UserFetchRepository $userFetchRepository, Mailer $mailer)
    {
        $payload = $message->payload();

        $userId = $payload['data']['user_id'];

        $user = $userFetchRepository->fetchById($userId);

        $userActivationCode = $payload['data']['activation_code'];

        $this->sendEmail($userActivationCode, $user, $mailer);
    }

    private function sendEmail(string $activationCode, User $user, Mailer $mailer)
    {
        $body = sprintf($this->bodyTemplate, $activationCode);

        $mailer->raw($body, function (Message $message) use ($user) {
            $message
                ->to($user->email())
                ->subject($this->subject);
        });
    }
}
