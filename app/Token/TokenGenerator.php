<?php


namespace App\Token;


interface TokenGenerator
{
    /**
     * @return string
     * @throws TokenGeneratorException
     */
    function generate(): string;
}
