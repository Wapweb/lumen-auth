<?php


namespace Tests\User;


use App\User;
use App\User\UserAlreadyConfirmedException;
use Tests\TestCase;

class UserTest extends TestCase
{
    private User $waitingUser;

    private user $confirmedUser;

    public function setUp(): void
    {
        parent::setUp();

        $this->waitingUser = $this->createTestUser();

        $this->confirmedUser = $this->createTestUser(true);
    }

    /**
     * @throws UserAlreadyConfirmedException
     */
    public function testWaitingUserShouldAbleToBecomeConfirmed()
    {
        $this->waitingUser->confirm();

        $confirmed = $this->waitingUser->isConfirmed();

        $this->assertTrue($confirmed);
    }

    /**
     * @throws UserAlreadyConfirmedException
     */
    public function testConfirmedUserShouldNotBeAbleToConfirm()
    {
        $this->expectException(UserAlreadyConfirmedException::class);
        $this->confirmedUser->confirm();
    }
}
