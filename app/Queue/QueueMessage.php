<?php


namespace App\Queue;


class QueueMessage
{
    private ?array $payload = null;

    private ?string $id;

    public function __construct(array $payload = null, string $id = null)
    {
        $this->id = $id;
        $this->payload = $payload;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function payload(): ?array
    {
        return $this->payload;
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }
}
