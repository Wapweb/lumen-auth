<?php


namespace App\Id\Generator;


use App\Id\Id;
use App\Id\IdGenerator;
use App\Id\SimpleId;

final class PhpUniqueIdGenerator implements IdGenerator
{
    public function generate(): string
    {
        return uniqid('', true);
    }
}
