<?php

namespace App;

use App\User\UserAlreadyConfirmedException;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class User implements AuthenticatableContract, AuthorizableContract
{
    private const STATUS_WAITING = 0;

    private const STATUS_CONFIRMED = 1;


    use Authenticatable, Authorizable;

    private string $id;

    private string $email;

    private string $password;

    private int $status = self::STATUS_WAITING;

    public function __construct(string $id, string $email, string $password, bool $status = self::STATUS_WAITING)
    {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->status = $status;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function password(): string
    {
        return  $this->password;
    }

    public function isConfirmed(): bool
    {
        return $this->status === self::STATUS_CONFIRMED;
    }

    public function status(): int
    {
        return  $this->status;
    }

    /**
     * @throws UserAlreadyConfirmedException
     */
    public function confirm()
    {
        if ($this->isConfirmed()) {
            throw new UserAlreadyConfirmedException();
        }

        $this->status = self::STATUS_CONFIRMED;
    }
}
