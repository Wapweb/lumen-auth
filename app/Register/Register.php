<?php


namespace App\Register;


use App\Id\IdGenerator;
use App\PasswordHash\PasswordHashGenerator;
use App\User;
use App\User\Repository\UserPersistRepository;
use App\User\Repository\UserFetchRepository;
use App\User\Repository\UserNotFoundException;
use App\UserMappers\RegisterRequestToUserMapper;

final class Register
{
    private RegisterRequestValidator $validator;

    private IdGenerator $idGenerator;

    private UserPersistRepository $userCreateRepository;

    private RegisterRequestToUserMapper $requestToUserMapper;

    private UserFetchRepository $userFetchRepository;

    private PasswordHashGenerator $hashGenerator;

    public function __construct(
        RegisterRequestValidator $validator,
        IdGenerator $idGenerator,
        UserPersistRepository $userCreateRepository,
        RegisterRequestToUserMapper $requestToUserMapper,
        UserFetchRepository $userFetchByIdRepository,
        PasswordHashGenerator $passwordHashGenerator
    )
    {
        $this->validator = $validator;
        $this->idGenerator = $idGenerator;
        $this->userCreateRepository = $userCreateRepository;
        $this->requestToUserMapper = $requestToUserMapper;
        $this->userFetchRepository = $userFetchByIdRepository;
        $this->hashGenerator = $passwordHashGenerator;
    }

    /**
     * @param RegisterRequest $registerRequest
     * @return User
     * @throws PersistenceException
     * @throws RequestValidationException
     */
    public function register(RegisterRequest $registerRequest): User
    {
        $this->validateRequest($registerRequest);

        $this->ensureUserNotExisted($registerRequest);

        return  $this->createUser($registerRequest);
    }

    /**
     * @param RegisterRequest $registerRequest
     * @throws RequestValidationException
     */
    private function ensureUserNotExisted(RegisterRequest $registerRequest)
    {
        try {
            $email = $registerRequest->email();

            $this->userFetchRepository->fetchByEmail($email);

            throw new RequestValidationException(sprintf('User with email `%s` already exists', $email));
        } catch (UserNotFoundException $notFoundException) {
            return;
        }
    }

    /**
     * @param RegisterRequest $registerRequest
     * @throws RequestValidationException
     */
    private function validateRequest(RegisterRequest $registerRequest)
    {
        $registerRequestValid = $this->validator->isValid($registerRequest);

        if (!$registerRequestValid) {
            throw new RequestValidationException($this->validator->getMessages());
        }
    }

    /**
     * @param RegisterRequest $registerRequest
     * @return User
     * @throws PersistenceException
     */
    private function createUser(RegisterRequest $registerRequest): User
    {
        $nextId = $this->idGenerator->generate();

        $passwordHash = $this->hashGenerator->generate($registerRequest->password());

        $user = $this->requestToUserMapper->map($registerRequest, $nextId, $passwordHash);

        try {
            $this->userCreateRepository->persist($user);
        } catch (\Exception $e) {
            throw new PersistenceException($e->getMessage());
        }

        return  $user;
    }
}
