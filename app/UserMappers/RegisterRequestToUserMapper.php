<?php


namespace App\UserMappers;


use App\Id\Id;
use App\Register\RegisterRequest;
use App\User;

class RegisterRequestToUserMapper
{
    public function map(RegisterRequest $registerRequest, string $id, string $hash): User
    {
        $email = $registerRequest->email();

        return new User($id, $email, $hash);
    }
}
