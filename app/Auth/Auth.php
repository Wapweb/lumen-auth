<?php


namespace App\Auth;


interface Auth
{
    /**
     * @param array|null $cred
     * @return bool
     * @throws AuthException
     */
    function authenticate(array $cred = null): bool;

    /**
     * @return mixed
     */
    function getIdentity();
}
