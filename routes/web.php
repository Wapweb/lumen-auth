<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var $router Router */

use Laravel\Lumen\Routing\Router;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/users', 'RegisterController@register');

$router->get('/users/activation/{code}', 'UserActivationController@verify');

$router->post('/users/login', 'AuthController@auth');

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->get('/users/{id}', 'UsersController@fetchOne');
});
