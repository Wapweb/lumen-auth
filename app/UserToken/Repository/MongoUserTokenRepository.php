<?php


namespace App\UserToken\Repository;


use App\UserActivation\Repository\UserActivationCodePersistException;
use App\UserToken\UserToken;
use App\UserToken\UserTokenFactory;
use Jenssegers\Mongodb\Connection;
use MongoDB\BSON\UTCDateTime;

class MongoUserTokenRepository implements UserTokenRepository
{
    private Connection $connection;

    private string $collectionName = 'user_token';

    private UserTokenFactory $userTokenFactory;

    public function __construct(Connection $connection, UserTokenFactory $userTokenFactory)
    {
        $this->connection = $connection;
        $this->userTokenFactory = $userTokenFactory;
    }

    /**
     * @param string $token
     * @return UserToken
     * @throws UserTokenNotFound
     */
    public function fetchByToken(string $token): UserToken
    {
        $data = $this->connection->collection($this->collectionName)->where('token', $token)->first();

        if (empty($data)) {
            throw new UserTokenNotFound(sprintf('User token by token `%s` not found', $token));
        }

        return $this->mapToModel($data);
    }

    public function persist(UserToken $userToken): bool
    {
        $data = $this->mapToData($userToken);

        $result = $this->connection->collection($this->collectionName)
            ->where('user_id', $userToken->userId())
            ->where('token', $userToken->token())
            ->update($data, ['upsert' => true]);

        if (!$result) {
            throw new UserTokenPersistException('Failed to persist user token');
        }

        return true;
    }

    /**
     * @param string $userId
     * @param string $token
     * @return UserToken
     * @throws UserTokenNotFound
     */
    public function fetchByUserIdAndToken(string $userId, string $token): UserToken
    {
        $data = $this->connection->collection($this->collectionName)
            ->where('user_id', $userId)
            ->where('token', $token)
            ->first();

        if (empty($data)) {
            throw new UserTokenNotFound(
                sprintf('User token by user `%s` and token `%s` not found', $userId, $token)
            );
        }

        return $this->mapToModel($data);
    }

    private function mapToModel(array $data): UserToken
    {
        /** @var \DateTime $expiredAt */
        $expiredAt = !empty($data['expired_at']) ? $data['expired_at']->toDateTime() : null;

        return  $this->userTokenFactory->create($data['token'], $data['user_id'], $expiredAt);
    }

    private function mapToData(UserToken $token)
    {
        $expiredAt = $token->expiredAt();

        return [
            'token' => $token->token(),
            'user_id' => $token->userId(),
            'expired_at' => empty($expiredAt) ? null : new UTCDateTime($expiredAt->getTimestamp() * 1000)
        ];
    }
}
