<?php


namespace App\Http\Controllers;

use App\User\Repository\UserFetchRepository;
use App\User\Repository\UserNotFoundException;
use App\UserMappers\UserToUserJsonResponseMapper;

class UsersController extends Controller
{
    private UserToUserJsonResponseMapper $userJsonMapper;

    private UserFetchRepository $userFetchByIdRepository;

    public function __construct(
        UserToUserJsonResponseMapper $userJsonMapper,
        UserFetchRepository $userFetchByIdRepository
    ) {
        $this->userJsonMapper = $userJsonMapper;
        $this->userFetchByIdRepository = $userFetchByIdRepository;
    }

    public function fetchOne($id)
    {
        try {
            $user = $this->userFetchByIdRepository->fetchById($id);

            $response = $this->userJsonMapper->map($user);

            return response()->json($response);
        } catch (UserNotFoundException $e) {
            return response()->json(['message' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
}
