<?php


namespace App\PasswordHash;


interface PasswordHashVerify
{
    function verify(string $password, string $hash): bool;
}
