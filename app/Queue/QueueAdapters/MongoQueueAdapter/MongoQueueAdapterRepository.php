<?php


namespace App\Queue\QueueAdapters\MongoQueueAdapter;


use App\Queue\QueueMessage;
use Jenssegers\Mongodb\Connection;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;

class MongoQueueAdapterRepository
{
    private Connection $connection;

    private string $collectionName = 'queue';

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param int $limit
     * @return QueueMessage[]
     */
    public function fetch(int $limit): array
    {
        $messages = $this->connection
            ->collection($this->collectionName)
            ->limit($limit)
            ->orderBy('created_at', -1)
            ->get()
            ->toArray();

        $result = [];

        $idsToDelete = [];

        foreach ($messages as $message) {
            $result[] = $this->mapToModel($message);

            $idsToDelete[] = $message['_id'];
        }

        if (!empty($idsToDelete)) {
            $this->connection->collection($this->collectionName)
                ->whereIn('_id', $idsToDelete)
                ->delete();
        }

        return  $result;
    }

    public function add(QueueMessage $message)
    {
        $data = $this->mapToData($message);

        $this->connection
            ->collection($this->collectionName)
            ->insert($data);
    }

    private function mapToModel(array $data)
    {
        $payload = !empty($data['payload']) ? json_decode($data['payload'], true) : null;
        /** @var ObjectId $id */
        $id = $data['_id'];

        return new QueueMessage($payload, $id->__toString());
    }

    private function mapToData(QueueMessage $message)
    {
        $payload = $message->payload();
        $payload = !empty($payload) ? json_encode($payload) : null;

        $createdAt = new UTCDateTime((new \DateTime())->getTimestamp() * 1000);

        return [
            'payload' => $payload,
            'created_at' => $createdAt
        ];
    }
}
